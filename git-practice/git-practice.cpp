// git-practice.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

static const char *INPUT_FILE = "input.txt";
static const char *OUTPUT_FILE = "output.txt";

template <typename T>
struct functor
{
public:
	T operator()(T& stuff)
	{
		return stuff++;
	}
};


template <typename T>
struct predicate
{
public:
	bool operator()(const T& arg1)
	{
			return arg1<0;
	}
};


template <typename T>
struct printer
{
public:
	void operator()(const T& stuff_to_print)
	{
		std::cout<<stuff_to_print<<std::endl;
	}
};


template <typename T>
class input_reader 
{
public:
	//TODO: do this cpp11 way
	std::vector<T> operator()(const char* input)
	{
		std::fstream F;
		F.open(input);
		std::vector<T> source_v;
		int k;
		while (!F.eof())
		{
			F>>k;
			source_v.push_back(k);
		}
		F.close();
		return source_v;
	}
};

template <typename T>
class output_writer
{
public:
		void operator()(const char* filename, const std::vector<T> vector_to_put_out)
        {
            //TODO: do this cpp11 way
            std::ofstream output_file(filename, std::ofstream::trunc);
            for (unsigned int i=0; i<vector_to_put_out.size(); i++)
            {
                output_file<<vector_to_put_out[i]<<std::endl;
            }
	    }

};

int _tmain(int argc, _TCHAR* argv[])
{
    std::vector<int> source_v;

    using curr_type = std::remove_reference<decltype(source_v[0])>::type;

    std::vector<curr_type> target_v;

    input_reader<curr_type> read;
    source_v = read(INPUT_FILE);

    predicate<curr_type> pred;
    std::copy_if(source_v.begin(), source_v.end(), std::back_inserter(target_v), pred);

    functor<curr_type> func;
    std::for_each(target_v.begin(), target_v.end(), func);

    printer<curr_type> printr;
    std::for_each(target_v.begin(), target_v.end(), printr);

    output_writer<curr_type> write;
    write(OUTPUT_FILE, target_v);

    return 0;
}
